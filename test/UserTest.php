<?php

namespace App\Test;

use App\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
  public function testTelName()
  {
    $user = new User(20, "Scarlett");
    $this->assertEquals("My name is Scarlett.", $user->tellName());
  }

  public function testTelAge()
  {
    $user = new User(20, "Scarlett");
    $this->assertEquals("I am 20 years old.", $user->tellAge());
  }

  public function testAddFavoriteMovie()
  {
    $user = new User(20, "Scarlett");
    $user->addFavoriteMovie("The Shawshank Redemption");
    $this->assertEquals(["The Shawshank Redemption"], $user->favoriteMovies);
  }

  public function testRemoveFavoriteMovie()
  {
    $user = new User(20, "Scarlett");
    $user->addFavoriteMovie("The Shawshank Redemption");
    $user->removeFavoriteMovie("The Shawshank Redemption");
    $this->assertEquals([], $user->favoriteMovies);
  }

  public function testRemoveFavoriteMovieThrowsException()
  {
    $user = new User(20, "Scarlett");
    $this->expectException(\InvalidArgumentException::class);
    $user->removeFavoriteMovie("The Shawshank Redemption");
  }

  public function testRemoveFavoriteMovieThrowsExceptionWithCorrectMessage()
  {
    $user = new User(20, "Scarlett");
    $this->expectExceptionMessage("Unknown movie: The Shawshank Redemption");
    $user->removeFavoriteMovie("The Shawshank Redemption");
  }
}
